$(document).ready(function() {

	var count = -1;
	var isThoughtSelected = false;
	var thoughts = [];
	$('#thought').mouseenter(function(){isThoughtSelected=true;});
	$('#thought').mouseleave(function(){isThoughtSelected=false;});
	$('#thought').on('wheel', function(e) {
		var delta = e.originalEvent.deltaY;
		if (delta > 0) displayThought(-1);
		else displayThought(1);
		return false;
	});
	var self = this;
	$.ajax(externalFeedUrl, {
		accepts:{
			xml:"application/rss+xml"
		},
		dataType:"xml",
		success:function(data) {
			var thoughts = [];
			$(data).find("entry").each(function () {
				var el = $(this);
				var thoughtHtml = el.find("content").text();
				var thoughtPlain = $("<div/>").html(thoughtHtml).text();
				var link = el.find("id").text();
				var noReshare = (el.find("author").text() === '');
				var noReply = (thoughtPlain.charAt(0) !== '@');
				if (noReply && noReshare) {
					var thought = [];
					thought['text'] = thoughtPlain;
					thought['link'] = link;
					thoughts.push(thought);
				}
			});
			setThoughts(thoughts);
			startTimer();
		}
	});

	function setThoughts(newThoughts) {
		self.thoughts = newThoughts;
	}

	function rotateThoughts() {
		if (isThoughtSelected === false) {
			displayThought(1);
		}
	}

	function displayThought(offset) {
		count = count + offset;
		if (count >= self.thoughts.length) {
			count = 0;
		} else if (count < 0) {
			count = self.thoughts.length - 1;
		}
		var $newThought = '<a href="' + self.thoughts[count]['link'] + '" title="Latest thought... Click to like, reshare or comment on Mastodon!"><blockquote>' + self.thoughts[count]['text'] + '</blockquote></a>'
		document.getElementById('thought').innerHTML = $newThought;
	}

	function startTimer(thoughts, links) {
		if (self.thoughts.length >= 1) {
		        setInterval(function() { rotateThoughts(); }, 6000);
                }
	}

});
