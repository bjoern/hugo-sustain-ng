<?php
# needs to be placed here: ~/mastodon.feed/config.feed.php
$config = [
    '0' => [
       'mastodon-instance' => 'https://mastodon.social',
       'cachePath' => '/path/where/feed/should/be/cached/',
       'user-id' => <insert-user-id>,
       'threshold' => 300,
       'token' => '<insert-access-token>',
    ],
    '1' => [
        'mastodon-instance' => 'https://mastodon.schiessle.org',
        'cachePath' => '/path/where/feed/should/be/cached/',
        'user-id' => <insert-user-id>,
        'threshold' => 300,
        'token' => '<insert-access-token>',
    ]
 ];
