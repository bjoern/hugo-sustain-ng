$(document).ready(function() {

    var count = -1;
    var isThoughtSelected = false;
    var thoughts = [];
    $('#thought').mouseenter(function(){isThoughtSelected=true;});
    $('#thought').mouseleave(function(){isThoughtSelected=false;});
    $('#thought').on('wheel', function(e) {
        var delta = e.originalEvent.deltaY;
        if (delta > 0) displayThought(-1);
        else displayThought(1);
        return false;
    });
    var self = this;
    $.ajax('https://www.schiessle.org/mastodon.feed/getmastodonfeed.php', {
        success:function(data) {
            setThoughts([]);
            if (Object.keys(data).length > 0) {
                setThoughts(data);
                startTimer();
            }
        }
    });

    function setThoughts(newThoughts) {
        self.thoughts = newThoughts;
    }

    function rotateThoughts() {
        if (isThoughtSelected === false) {
            displayThought(1);
        }
    }

    function displayThought(offset) {
        count = count + offset;
        if (count >= Object.keys(self.thoughts).length) {
            count = 0;
        } else if (count < 0) {
            count = Object.keys(self.thoughts).length - 1;
        }
        var $newThought = '<a href="' + self.thoughts[count]['url'] + '" title="Latest thought... Click to like, reshare or comment on Mastodon!"><blockquote>' + self.thoughts[count]['content'] + '</blockquote></a>'
        document.getElementById('thought').innerHTML = $newThought;
    }

    function startTimer(thoughts, links) {
        if (Object.keys(self.thoughts).length >= 1) {
            setInterval(function() { rotateThoughts(); }, 6000);
        }
    }

});
